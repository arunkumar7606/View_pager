package com.arun.aashu.imageslider;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by aAsHu on 4/19/2018.
 */

public class MyAdapter extends PagerAdapter {


    private ArrayList<Integer> images;
    private LayoutInflater inflater;
    private Context context;


    public MyAdapter(ArrayList<Integer> images, Context context) {
        this.images = images;
//        this.inflater = inflater;
        inflater=LayoutInflater.from(context);
        this.context = context;
    }


    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
        container.removeView((View)object);

    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
//        return super.instantiateItem(container, position);

        View v=inflater.inflate(R.layout.slide,container,false);

        ImageView myImage=(ImageView)v.findViewById(R.id.image);
        myImage.setImageResource(images.get(position));

        container.addView(v,0);

        return  v;
    }
}
