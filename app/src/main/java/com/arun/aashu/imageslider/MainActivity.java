package com.arun.aashu.imageslider;

import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends AppCompatActivity {

    private static ViewPager mpager;
    private static int currentPage=0;
    private static final Integer[] pic={

            R.drawable.a,
            R.drawable.b,
            R.drawable.c,
            R.drawable.d,
            R.drawable.e,
            R.drawable.f,
            R.drawable.g,
            R.drawable.h

    };

    ArrayList<Integer> picArray=new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        jadu();

    }

    public void jadu(){

        for (int i=0;i<pic.length;i++)
            picArray.add(pic[i]);

        mpager= findViewById(R.id.pager);
        mpager.setAdapter(new MyAdapter(picArray,this));

        CircleIndicator indicator=findViewById(R.id.indicator);
        indicator.setViewPager(mpager);

        final Handler handler=new Handler();

        final  Runnable update=new Runnable() {
            @Override
            public void run() {
                if (currentPage==pic.length){
                    currentPage=0;
                }
                mpager.setCurrentItem(currentPage++,true);
            }
        };

        Timer swipeTimer= new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(update);
            }
        },3000,3000);
    }
}
